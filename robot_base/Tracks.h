#ifndef Tracks_h
#define Tracks_h

#include "Arduino.h"

const short MIN_SPEED = 0;
const short MAX_SPEED = 255;
const short MAX_TURN_ANGLE = 180;

const byte LM_F_IDX = 0;
const byte LM_B_IDX = 1;
const byte RM_F_IDX = 2;
const byte RM_B_IDX = 3;

class Tracks
{
  public:
    Tracks(int lfPin, int lbPin, int rfPin, int rbPin);
    void move(short moveSpeed, short turnAngle);
    void test();
  private:
    int _lfPin;
    int _lbPin;
    int _rfPin;
    int _rbPin;
    void setMotorsSpeed(short speeds[]);
    short revertDirection(short idx);
    short turnSpeed(short moveSpeed, short turnAngle);
};

#endif
