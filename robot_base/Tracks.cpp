#include "Arduino.h"
#include "Tracks.h"

Tracks::Tracks(int lfPin, int lbPin, int rfPin, int rbPin){
  pinMode(lfPin, OUTPUT);
  pinMode(lbPin, OUTPUT);
  pinMode(rfPin, OUTPUT);
  pinMode(rbPin, OUTPUT);
  _lfPin = lfPin;
  _lbPin = lbPin;
  _rfPin = rfPin;
  _rbPin = rbPin;
}

void Tracks::move(short moveSpeed, short turnAngle) {
  short speeds[4] = {0, 0, 0, 0};
  short lm, rm, lmSpeed, rmSpeed;
  if (moveSpeed > 0){
    lm = LM_F_IDX;
    rm = RM_F_IDX;
  } else {
    lm = LM_B_IDX;
    rm = LM_B_IDX;
  }
  if (turnAngle == 0){
    lmSpeed = rmSpeed = abs(moveSpeed);
  } else if (turnAngle > 0) {
    if (abs(turnAngle) > (MAX_TURN_ANGLE / 2)) {
      rm = revertDirection(rm);
    }
    lmSpeed = abs(moveSpeed);
    rmSpeed = turnSpeed(moveSpeed, abs(turnAngle));
  } else {
    if (abs(turnAngle) > (MAX_TURN_ANGLE / 2)) {
      lm = revertDirection(lm);
    }
    lmSpeed = turnSpeed(moveSpeed, abs(turnAngle));
    rmSpeed = abs(moveSpeed);
  }
  speeds[lm] = lmSpeed;
  speeds[rm] = rmSpeed;
  setMotorsSpeed(speeds);
}

short Tracks::turnSpeed(short moveSpeed, short turnAngle) {
  if (turnAngle < MAX_TURN_ANGLE / 2) {
    return map(turnAngle, 0, MAX_TURN_ANGLE / 2, moveSpeed, 0);
  } else {
    return map(turnAngle, MAX_TURN_ANGLE / 2, MAX_TURN_ANGLE, 0, moveSpeed);
  }
}

short Tracks::revertDirection(short idx) {
  switch(idx) {
    case LM_F_IDX:
      return LM_B_IDX;
    case LM_B_IDX:
      return LM_F_IDX;
    case RM_F_IDX:
      return RM_B_IDX;
    case RM_B_IDX:
      return RM_F_IDX;
  }
}

void Tracks::setMotorsSpeed(short speeds[]) {
  analogWrite(_lfPin, speeds[LM_F_IDX]);
  analogWrite(_lbPin, speeds[LM_B_IDX]);
  analogWrite(_rfPin, speeds[RM_F_IDX]);
  analogWrite(_rbPin, speeds[RM_B_IDX]);
}

//used in setup - for demo moving
void Tracks::test() {
  //up to max forwars speed
  for (int i = MIN_SPEED; i <= MAX_SPEED; i+= 10){
    move(i, 0);
    delay(100);
  }
  //change speed and direction right side
  for (int i = 0; i <= MAX_TURN_ANGLE; i+= 2){
    move(MAX_SPEED, i);
    delay(100);
  }
  //retur right side to max forward speed
  for (int i = MAX_TURN_ANGLE; i >=0; i-= 2){
    move(MAX_SPEED, i);
    delay(100);
  }
  //change speed and direction left side
  for (int i = 0; i >= -MAX_TURN_ANGLE; i-= 2){
    move(MAX_SPEED, i);
    delay(100);
  }
  //retur left side to max forward speed
  for (int i = -MAX_TURN_ANGLE; i <=0; i+= 2){
    move(MAX_SPEED, i);
    delay(100);
  }
}
