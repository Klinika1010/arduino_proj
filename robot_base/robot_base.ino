#include "Tracks.h"
//motor 
#define LM_F_PIN 10
#define LM_B_PIN 11
#define RM_F_PIN 9
#define RM_B_PIN 6

Tracks tracks(LM_F_PIN, LM_B_PIN, RM_F_PIN, RM_B_PIN);

void setup() {
  //debug
  Serial.begin(9600);
  pinMode(A1, INPUT);
}

void loop() {
  Serial.println(analogRead(A1));
}
